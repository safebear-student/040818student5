package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by CCA_Student on 04/08/2018.
 */
public class LoginPage {

    WebDriver driver;

    //Find the username field
    @FindBy(id = "myid")
    WebElement username_field;

    //Find the password field
    @FindBy(id = "mypass")
    WebElement password_field;

    //Find both username and password fields by way of both their IDs containing 'my'
    @FindAll({
            @FindBy(xpath = "//input[contains(@id,'my')]")
    })
    List<WebElement> input_fields;

    public void login(String username, String password) {
        this.username_field.sendKeys(username);
        this.password_field.sendKeys(password);
        this.password_field.submit();
    }

    public LoginPage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage(){
        return driver.getTitle().contains("Sign In");
    }

    public boolean multielements(String test) {
        //For each of the WebElements in the input_fields list, send the String passed by variable 'test' (it's "testmulti").
        input_fields.forEach(s -> s.sendKeys(test));
        //Confirm that each field contains 'testmulti' or whatever String is passed
        return username_field.getAttribute("value").contains(test);
    }
}

