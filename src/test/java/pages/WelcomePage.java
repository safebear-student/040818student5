package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 04/08/2018.
 */
public class WelcomePage {
    WebDriver driver;

    @FindBy(linkText = "Login")
    WebElement loginLink;

    //@FindBy(headingText = "Welcome to Safebear")
    //WebElement strapLine;

    public WelcomePage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Welcome");
    }

//    public boolean checkCorrectHeader(){
//        return driver.get(String).startsWith("Welcome to Safebear");
//    }
    public void clickOnLogin(){
        loginLink.click();
    }
}