package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 04/08/2018.
 */
public class UserPage {
    WebDriver driver;

    public UserPage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage(){
        return driver.getTitle().contains("Logged In");
    }
}
