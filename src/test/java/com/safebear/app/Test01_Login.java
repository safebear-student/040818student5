package com.safebear.app;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/*
 * Created by CCA_Student on 04/08/2018.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin(){
        //Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 2 Click on the Login link and Login Page loads
        welcomePage.clickOnLogin();
        //Step 3 confirm that we're now on the Login page
        assertTrue(loginPage.checkCorrectPage());
        //Step 4 Login with valid credentials
        loginPage.login("testuser", "testing");
        //Step 5 Check that we're now on the User Page
        assertTrue(userPage.checkCorrectPage());
    }
}
