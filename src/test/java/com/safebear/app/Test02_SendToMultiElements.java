package com.safebear.app;

import org.junit.Test;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 05/08/2018.
 */


public class Test02_SendToMultiElements extends BaseTest {
@Test
    public void testMultiElements(){
    //Step 1 Confirm we're on the welcome page
    assertTrue(welcomePage.checkCorrectPage());
    //Step 2 Click on Login
    welcomePage.clickOnLogin();
    //Step 3 Identify all the input fields and populate them with 'testmulti'
    loginPage.multielements("testmulti");
}
}
