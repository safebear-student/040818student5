package com.safebear.app;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pages.LoginPage;
import pages.WelcomePage;
import pages.UserPage;

import java.util.concurrent.TimeUnit;


/**
 * Created by CCA_Student on 04/08/2018.
 */

public class BaseTest {

    WebDriver driver;

    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;

    @Before
    public void setUp(){
        driver = new ChromeDriver();
        //ChromeOptions chromeOptions = new ChromeOptions();
        //chromeOptions.addArguments("headless");
        //this.driver = new ChromeDriver(chromeOptions);
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);
        driver.get("http://automate.safebear.co.uk/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After
    public void tearDown(){
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();

    }
}
